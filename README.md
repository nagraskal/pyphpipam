# PyphpIPAM
PyphpIPAM is a small library to get data from an ipam server via the built-in API

## Example
```python
from pyphpipam import PHPIpamAPI, PHPIpamAPIException

url = 'https://ipam.domain.com'
appid = 'ipam'
user = 'ipam'
password = 'SeCuRePaSsWoRd'
section = 'My Section'

# Login
api = PHPIpamAPI(url, appid)
api.login(user, password)

# Get section-id
section_id = api.get("sections/" + section + "/")["id"]

# Get subnet data
subnets = api.get("sections/" + section_id + "/subnets/")

# Get all ips
all_addresses = []
subnet_ids = [id for subnet in subnets]
for subnet_id in subnet_ids:
    try:
        all_addresses += api.get("subnets/" + subnet_id + "/addresses/")
    except:
        pass

```
