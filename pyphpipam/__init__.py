import logging
import requests
import json


class _NullHandler(logging.Handler):
    def emit(self, record):
        pass

logger = logging.getLogger(__name__)
logger.addHandler(_NullHandler())


class PHPIpamAPIException(Exception):
    """ generic phpipam api exception
    code list:


    """
    pass


class PHPIpamAPI(object):
    def __init__(self,
                server='http://localhost/phpipam',
                appid=None,
                token=None,
                session=None,
                timeout=None):

        if session:
            self.session = session
        else:
            self.session = requests.Session()

        # Default headers for all requests
        self.session.headers.update({
            'User-Agent': 'python/pyphpipam',
            'Cache-Control': 'no-cache',
            'Content-Type': 'application/json'
        })

        self.auth = ''
        self.id = 0
        self.timeout = timeout

        self.url = server + '/api/' + appid + '/'
        logger.info("JSON-RPC Server Endpoint: %s", self.url)

    def login(self, user='', password=''):
        """Convenience method for calling user.authenticate and storing the resulting auth token
           for further commands.
           :param password: Password used to login into phpipam
           :param user: Username used to login into phpipam
        """
        
        response = self.session.post(
            self.url+'/user/',
            auth = (user,password)
        )
        logger.debug("Response Code: %s", str(response.status_code))
        response.raise_for_status()
        
        self.token = response.json()['data']['token']
        if not len(self.token):
            raise PHPIpamAPIException("Received empty token")
        
        self.session.headers.update({
            'token': self.token
        })


    def get(self, path):
        get_url = self.url + path
        response = self.session.get(
            get_url,
            timeout=self.timeout
        )
        logger.debug("Response Code: %s", str(response.status_code))
        response.raise_for_status()
        
        if not len(response.text):
            raise PHPIpamAPIException("Received empty response")

        try:
            response_json = json.loads(response.text)
        except ValueError:
            raise PHPIpamAPIException(
                "Unable to parse json: %s" % response.text
            )
        logger.debug("Response Body: %s", json.dumps(response_json,
                                                     indent=4,
                                                     separators=(',', ': ')))

        if 'error' in response_json:  # some exception
            if 'data' not in response_json['error']:  # some errors don't contain 'data': workaround for ZBX-9340
                response_json['error']['data'] = "No data"
            msg = u"Error {code}: {message}, {data}".format(
                code=response_json['error']['code'],
                message=response_json['error']['message'],
                data=response_json['error']['data']
            )
            raise PHPIpamAPIException(msg, response_json['error']['code'])
        
        if "data" in response_json:
            return(response_json["data"])
        else:
            return ""
            
